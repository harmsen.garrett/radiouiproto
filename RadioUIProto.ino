#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>
#include "Adafruit_DRV2605.h"

Adafruit_DRV2605 drv;

Adafruit_SH110X display = Adafruit_SH110X(64, 128, &Wire);



  int channelNumber1 = 10; 
  int channelNumber2 = 5;
  String screenString = "None";
  bool isOn = false; 

void setup() {
  Serial.begin(115200);

  //haptic
  drv.begin();
  drv.selectLibrary(1);

  Serial.println("128x64 OLED FeatherWing test");
  display.begin(0x3C, true); // Address 0x3C default

  Serial.println("OLED begun");

  // Show image buffer on the display hardware.
  // Since the buffer is intialized with an Adafruit splashscreen
  // internally, this will display the splashscreen.
  display.display();
  delay(1000);

  // Clear the buffer.
  display.clearDisplay();
  display.display();

  display.setRotation(1);
 

  // text display tests
  display.setTextSize(5);
  display.setTextColor(SH110X_WHITE);
  display.setCursor(20,10);
  //display.print(screenString);
  display.display();


  //setup pins
  //rotary switch
  pinMode(5,INPUT_PULLUP);
  pinMode(6,INPUT_PULLUP);
  pinMode(9,INPUT_PULLUP);
  pinMode(10,INPUT_PULLUP);
  //+/- switch
  pinMode(11,INPUT_PULLUP);
  pinMode(12,INPUT_PULLUP);
  //on off switch
  pinMode(13,INPUT_PULLUP);
  //PTT switch
  pinMode(A2,INPUT_PULLUP);

  //LEDs
  pinMode(A0,OUTPUT); //red
  pinMode(A1,OUTPUT); //blue

  //set LEDs off initially
  digitalWrite(A0,LOW);
  digitalWrite(A1,LOW);

  
  
}

//button time requirements
long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 500;    // the debounce time; increase if the output flickers
long powerSwitchStartHoldTime = 0;
long powerSwitchHoldThreshold = 1000;
bool powerSwitchHeld = false;
bool turningOnOff = false;  //only true when actively turning on or off

void loop() {
  int powerSwitch = !digitalRead(13);

  if (powerSwitch && !powerSwitchHeld) {//initial press of power switch
    Serial.println("reset power switch");
    powerSwitchStartHoldTime = millis();  
    powerSwitchHeld = true;
  }
  
  if (!powerSwitch && powerSwitchHeld){//power switch released,  reset timer
        Serial.println(" power switch released");

    powerSwitchStartHoldTime = 0; 
    powerSwitchHeld = false;
    turningOnOff = false; 
  }

  if (millis() - powerSwitchStartHoldTime > powerSwitchHoldThreshold && powerSwitchHeld) { //button held longer than threshold!  "turn on or off"  display a string and buzz while this is happening
    Serial.println(" power switch held past threshold");
    if (!turningOnOff) {
      isOn = !isOn;
      screenString = "OFF";
      if (isOn) {
       screenString = "ON"; 
      }
      display.clearDisplay(); // Address 0x3C default
    display.setCursor(20,10);
    display.print(screenString);
    display.display();
      
      // set the effect to play
    drv.setWaveform(0, 58);  // play effect 
    drv.setWaveform(1, 0);       // end waveform
  
    // play the effect!
    drv.go();
    delay(500);    
    }
    turningOnOff = true;
     Serial.println(isOn);
  }

  if (isOn) {
  //"ON" state
    //read switches -- ! put in front of each switch so that values are true when switch is selected
    int blueRotary = !digitalRead(6);
    int greenRotary = !digitalRead(10);
    int whiteRotary = !digitalRead(9);
    int yellowRotary = !digitalRead(5);
  
    int positiveSwitch = !digitalRead(11);
    int negativeSwitch = !digitalRead(12);
    
    
    int PTTSwitch = !digitalRead(A2);
  
    //Dial
    String dialIndicator;
    if(blueRotary) dialIndicator = "B";
    if(greenRotary) dialIndicator = "2";
    if(whiteRotary) dialIndicator = "1";
    if(yellowRotary) dialIndicator = "S";
    Serial.println(dialIndicator);
  
    
    
  
   String newScreenString;
   if (dialIndicator == "B") { // Bluetooth mode
      newScreenString = "B";
      digitalWrite(A0,LOW);
      digitalWrite(A1,HIGH);
   }
   else if (dialIndicator == "S") { //share mode
      newScreenString = "S";
      digitalWrite(A0,HIGH);
      digitalWrite(A1,HIGH);
   }
   else if (dialIndicator == "1") { //channel 1 mode
      //increase/decrease channel with debounce
      if (positiveSwitch && (millis()-lastDebounceTime) > debounceDelay) {
        lastDebounceTime = millis();
        channelNumber1 = channelNumber1+1;
      }
      if (negativeSwitch && (millis()-lastDebounceTime) > debounceDelay){
        lastDebounceTime = millis();
        channelNumber1 = channelNumber1-1;
      }
      
      newScreenString = channelNumber1;
      digitalWrite(A0,LOW);
      digitalWrite(A1,LOW);
   }
   else if (dialIndicator == "2") { //channel 2 mode
  
     //increase/decrease channel with debounce
      if (positiveSwitch && (millis()-lastDebounceTime) > debounceDelay) {
        lastDebounceTime = millis();
        channelNumber2 = channelNumber2+1;
      }
      if (negativeSwitch && (millis()-lastDebounceTime) > debounceDelay){
        lastDebounceTime = millis();
        channelNumber2 = channelNumber2-1;
      }
      
      newScreenString = channelNumber2;
      digitalWrite(A0,LOW);
      digitalWrite(A1,LOW);
   }
  
   
  
  
   //PTT
   if (PTTSwitch) {
    // set the effect to play
    drv.setWaveform(0, 58);  // play effect 
    drv.setWaveform(1, 0);       // end waveform
  
    // play the effect!
    drv.go();
    newScreenString = "TX";
    digitalWrite(A0,HIGH);
    delay(50);
    digitalWrite(A0,LOW);
    delay(50);
    
   }
    
  
    
  // update display if it's new! 
   if(newScreenString != screenString && newScreenString != "") {
    // set the effect to play
    drv.setWaveform(0, 52);  // play effect 
    drv.setWaveform(1, 0);       // end waveform
  
    // play the effect!
    drv.go();
    
    display.clearDisplay(); // Address 0x3C default
    display.setCursor(20,10);
    screenString = newScreenString;
    display.print(screenString);
   
    delay(100);
    yield();
    display.display();
   }
 
 }
 else { //off state! 
  digitalWrite(A0,LOW);
  digitalWrite(A1,LOW);
  display.clearDisplay();
  display.display();
 }
  
  
}
